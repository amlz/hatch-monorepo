from hatchling.plugin import hookimpl

from .build import MonorepoBuildHook
from .environment import MonorepoEnvironment
from .publish import MonorepoPublisher


@hookimpl
def hatch_register_environment():
    return MonorepoEnvironment

"""
@hookimpl
def hatch_register_build_hook():
    return MonorepoBuildHook


@hookimpl
def hatch_register_publisher():
    return MonorepoPublisher
"""