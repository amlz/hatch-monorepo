
from hatch.publish.plugin.interface import PublisherInterface


class MonorepoPublisher(PublisherInterface):
    PLUGIN_NAME = 'monorepo'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def get_option_types() -> dict:
        # TODO Anything to add?
        return {}
